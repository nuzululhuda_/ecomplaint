<?php

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Position::create([
            'position' => 'Rektor',
        ]);
        Position::create([
            'position' => 'Dekan Fakultas Saintek',
        ]);
        Position::create([
            'position' => 'Dekan Fakultas Psikologi',
        ]);
        Position::create([
            'position' => 'Dekan Fakultas Syariah',
        ]);
        Position::create([
            'position' => 'Dekan Fakultas Humaniora',
        ]);

    }
}
