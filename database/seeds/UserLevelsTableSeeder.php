<?php

use App\Models\UserLevel;
use Illuminate\Database\Seeder;

class UserLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserLevel::create([
            'level' => 'Mahasiswa'
        ]);

        UserLevel::create([
            'level' => 'Pegawai'
        ]);
    }
}
