<?php

use App\Models\StatusComplaint;
use Illuminate\Database\Seeder;

class StatusComplaintsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusComplaint::create([
            'status' => 'Open',
            'default_status' => true,
            'status_badge' => 'primary',
        ]);
        StatusComplaint::create([
            'status' => 'Answered',
            'default_status' => true,
            'status_badge' => 'warning',
        ]);
        StatusComplaint::create([
            'status' => 'danger',
            'default_status' => true,
            'status_badge' => 'primary',
        ]);
    }
}
