<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Nuzulul Huda',
            'email' => 'hudanuzulul@gmail.com',
            'password' => Hash::make('secret'),
            'user_level_id' => 1,
            'api_token' => str_random(50),
        ]);

        User::create([
            'name' => 'Ulul',
            'email' => 'ulul@gmail.com',
            'password' => Hash::make('secret'),
            'user_level_id' => 2,
            'api_token' => str_random(50),
            'position_id' => 2,
        ]);
    }
}
