<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('api_token')->nullable();
            $table->string('avatar')->nullable();
            $table->unsignedInteger('user_level_id');
            $table->unsignedInteger('position_id')->nullable();
            $table->foreign('user_level_id')
                    ->references('id')->on('user_levels')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('position_id')
                    ->references('id')->on('positions')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
