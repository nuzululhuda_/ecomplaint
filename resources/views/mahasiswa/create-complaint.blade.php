@extends('mahasiswa.layouts.app')

@section('css')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="row">
    <div class="col-sm-12">
        <section class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-block">
                        <h3 class="card-title">Form membuat komplain</h3>
                        <form class="form" action="{{ route('mahasiswa.store.complaint') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <input type="text" name="complaint_subject" class="form-control" placeholder="Apa yang ingin anda sampaikan">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div class="img-thumbnail">
                                        <img src="{{ asset('img/300x250.png') }}" id="img-upload" width="300px" height="250px">
                                    </div>
                                    <input type="file" class="form-control" accept="image/*" name="img" id="imgInp">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <textarea name="complaint_body"  cols="30" rows="10" class="form-control" placeholder="Masukkan detail keluhan anda"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-9">
                                  
                                    <select id="tujuan" name="tujuan[]" class="form-control" multiple="multiple">
                                        <option value=""></option>
                                        @foreach ($complaint_to as $to)
                                            <option value="{{ $to->position }}">{{ $to->position }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-grow row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
       
        
        
        <section class="row">
            <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
        </section>
    </div>
</section>
@endsection

@section('js')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
$(document).ready( function() {
    $(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    }); 

    $("#tujuan").select2({
        placeholder: "Pilih tujuan komplain"
    });

    
});
</script>
@endsection