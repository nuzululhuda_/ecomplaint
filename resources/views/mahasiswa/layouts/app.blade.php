<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="images/favicon.ico">
	<title>Dashboard Mahasiswa</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('dashboard/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    
    <!-- Icons -->
    <link href="{{ asset('dashboard/css/font-awesome.css') }}" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('dashboard/css/style.css') }}" rel="stylesheet">
    @yield('css')
    
</head>
<body>
	<div class="container-fluid" id="wrapper">
		<div class="row">

			@include('mahasiswa.layouts.sidebar')

			<main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
				<header class="page-header row justify-center">
					<div class="col-md-6 col-lg-8" >
						<h1 class="float-left text-center text-md-left">Dashboard</h1>
					</div>
					<div class="dropdown user-dropdown col-md-6 col-lg-4 text-center text-md-right"><a class="btn btn-stripped dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<div class="username mt-1">
							<h4 class="mb-1">{{ Auth::user()->name }}</h4>
							<h6 class="text-muted">Mahasiswa</h6>
						</div>
						</a>
						<div class="dropdown-menu dropdown-menu-right" style="margin-right: 1.5rem;" aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#"><em class="fa fa-user-circle mr-1"></em> View Profile</a>
						     <a class="dropdown-item" href="#"><em class="fa fa-sliders mr-1"></em> Preferences</a>
							 
							 <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
								
								<em class="fa fa-power-off mr-1"></em> {{ __('Logout') }}
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
							 
							
						</div>
					</div>
					<div class="clear"></div>
				</header>
				
				@yield('content')

			</main>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('dashboard/js/jquery-3.2.1.slim.min.js') }}" ></script>
    <script src="{{ asset('dashboard/js/popper.min.js') }}" ></script>
    <script src="{{ asset('dashboard/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('dashboard/js/custom.js') }}"></script>
    <script src="{{ asset('dashboard/js/custom.js') }}"></script>
    
    
    @yield('js')
    
    
	</body>
</html>
