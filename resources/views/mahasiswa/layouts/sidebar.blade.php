<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2">
        <h1 class="site-title">
            <a href="#">
                <em class="fa fa-rocket"></em> 
                Ecomplaint 
            </a>
        </h1>
                                            
        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>
        <ul class="nav nav-pills flex-column sidebar-nav">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                    <em class="fa fa-dashboard"></em> 
                    Dashboard 
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('mahasiswa.komplain') }}">
                    <em class="fa fa-dashboard"></em> 
                    Komplain saya 
                    <span class="sr-only">(current)</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link " href="{{ route('mahasiswa.create.complaint') }}">
                    <em class="fa fa-dashboard"></em> 
                    Buat Komplain 
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            
        </ul>
        {{-- <a href="login.html" class="logout-button"><em class="fa fa-power-off"></em> Signout</a> --}}
        <a class="logout-button" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
            <em class="fa fa-power-off"></em>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </nav>