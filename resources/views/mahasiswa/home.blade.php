@extends('mahasiswa.layouts.app')

@section('content')
<section class="row">
        <div class="col-sm-12">
            <section class="row">
                <div class="col-md-12 col-lg-8">
                    
                    
                    <div class="card mb-4">
                        <div class="card-block">
                            <h3 class="card-title">Komplain Saya</h3>
                           
                            <div class="divider" style="margin-top: 1rem;"></div>
                            <div class="articles-container">                               
                                @forelse ($complaints as $complaint)
                                    
                                    <div class="article border-bottom">
                                        <div class="col-xs-12">
                                            <div class="row">
                                            <div class="col-2 date">
                                                {{-- <div class="large">{{ Carbon\Carbon::parse($complaint->created_at)->format('d') }}
                                                    </div>
                                                <div class="text-muted">{{ Carbon\Carbon::parse($complaint->created_at)->format('M') }}</div> --}}
                                                <img src="{{ Storage::url('image/thumbnail/'.$complaint->complaint_image) }}" class="img img-thumbnail" width="300px" height="250px">
                                            </div>
                                            <div class="col-10">
                                                <h4>
                                                    <a href="{{ route('mahasiswa.show.complaint', $complaint->id) }}">
                                                        {!! $complaint->complaint_subject !!}
                                                    </a>
                                                </h4>
                                                <p>{{ substr(strip_tags($complaint->complaint_body), 0, 100) }}{{ strlen(strip_tags($complaint->complaint_body)) > 50 ? "..." : "" }}</p>
                                                <h6 class="blockquote-footer text-left">
                                                        {{ $complaint->name }}, {{ \Carbon\Carbon::parse($complaint->created_at)->diffForHumans() }}
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    </div> 
                                @empty
                                    Belum ada komplain
                                @endforelse


                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
            {{ $complaints->links() }}
            <section class="row">
                <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
            </section>
        </div>
    </section>
@endsection