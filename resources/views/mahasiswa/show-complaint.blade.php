@extends('mahasiswa.layouts.app')

@section('content')
<div class="card mb-4">
    <div class="card-block">
        <div class="divider" style="margin-top: 1rem;"></div>
        <div class="articles-container">
            <div class="article border-bottom">
                <div class="col-xs-12">
                    <h3 class="card-title text-center">{{ \strip_tags($complaint->complaint_subject) }}</h3>
                    <div class="row">
                        <img src="{{ Storage::url('image/thumbnail/'.$complaint->complaint_image) }}" class="img img-thumbnail center-block img-responsive ">
                    </div>
                    <p>
                        {!! $complaint->complaint_body !!}
                    </p>
                    <h6 class="blockquote-footer text-left mt-3">
                            @if ($complaint->avatar)
                                <img src="{{ Storage::url($complaint->avatar) }}" class="rounded-circle" style="width: 30px;"> 
                            @else
                                <img src="{{ url('/img/ava.png') }}" style="width: 30px;">
                            @endif
                            {{ $complaint->name }}, {{ \Carbon\Carbon::parse($complaint->created_at)->diffForHumans() }}
                    </h6>
                    
                </div>
                <div class="clear"></div>
            </div><!--End .article-->
            <div class="container p-5">
                <h3>Comments</h3>
                <hr>
                @if (Auth::user())
                    @if (Auth::user()->avatar)
                        <img src="{{ Storage::url(Auth::user()->avatar) }}" class="rounded-circle" style="width: 30px;">
                    @else
                        <img src="{{ url('/img/ava.png') }}" class="rounded-circle" style="width: 30px;">
                    @endif
                    {{ Auth::user()->name }}
                    <form class="mt-2" method="POST" action="{{ route('comment.store', $complaint->id) }}">
                        @csrf
                        <div class="form-group">
                            <textarea name="comment_body" class="form-control" style="resize: none;"></textarea>
                        </div>
                        
                        @if (Auth::user()->position_id != null)
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Ubah status komplain
                                    </div>
                                    <div class="col-md-9">
                                        <select name="status_complaint" class="form-control">
                                            @foreach ($status as $item)
                                                <option value="{{ $item->id }}">{{ $item->status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>    
                        @endif
                        <button type="submit" class="btn btn-primary mt-2">Publish</button>
                    </form>
                @else
                    <div class="col">
                        <a href="#">
                        <button  class="btn btn-outline-info mt-2  mr-3" type="button">
                            Sign In
                        </button>
                        </a>
                        <span class="mt-5 mr-3">or</span> 
                        <a href="#">
                        <button  class="btn btn-outline-info mt-2  mr-3" type="button">
                            Register
                        </button>
                        </a> to comment
                    </div>
                @endif
                <div class="container-fluid mt-5">
                                 
                @foreach ($comments AS $comment)
                    <div class="alert alert-light border-primary" role="alert">
                        <p>
                        @if ($comment->avatar)
                            <img src="{{ Storage::url($comment->avatar) }}" class="rounded-circle" style="width: 30px;"> 
                        @else
                            <img src="{{ url('/img/ava.png') }}" style="width: 30px;">
                        @endif

                        {{ $comment->name }} 
                       
                        @if ($comment->position == 'Mahasiswa')
                            <span class="badge badge-primary">{{ $comment->position }}</span></p>
                        @else 
                            <span class="badge badge-warning">{{ $comment->position }}</span></p>    
                        @endif
                        <p>
                            {{ $comment->comment_body }}
                        </p>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection