@extends('pegawai.layouts.app')

@section('content')
<section class="row">
        <div class="col-sm-12">
            <section class="row">
                <div class="col-md-12 col-lg-8">
                    
                    
                    <div class="card mb-4">
                        <div class="card-block">
                            <h3 class="card-title">Semua Komplain</h3>
                            <div class="dropdown card-title-btn-container">
                                <button class="btn btn-sm btn-subtle" type="button"><em class="fa fa-list-ul"></em> View All</button>
                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                            </div>
                            <div class="divider" style="margin-top: 1rem;"></div>

                            <div class="articles-container">                               
                                @forelse ($unanswered_complaints as $complaint)
                                    
                                    <div class="article border-bottom">
                                        <div class="col-xs-12">
                                            <div class="row">
                                            <div class="col-2 date">
                                                {{-- <div class="large">{{ Carbon\Carbon::parse($complaint->created_at)->format('d') }}
                                                    </div>
                                                <div class="text-muted">{{ Carbon\Carbon::parse($complaint->created_at)->format('M') }}</div> --}}
                                                <img src="{{ Storage::url('image/thumbnail/'.$complaint->complaint_image) }}" class="img img-thumbnail" width="300px" height="250px">
                                            </div>
                                            <div class="col-10">
                                                <h4>
                                                    <a href="{{ route('pegawai.show.complaint', $complaint->id) }}">
                                                        {!! $complaint->complaint_subject !!}
                                                    </a>
                                                </h4>
                                                <p>{{ substr(strip_tags($complaint->complaint_body), 0, 100) }}{{ strlen(strip_tags($complaint->complaint_body)) > 50 ? "..." : "" }}</p>
                                                <h6 class="blockquote-footer text-left">
                                                        {{ $complaint->name }}, {{ \Carbon\Carbon::parse($complaint->created_at)->diffForHumans() }}
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    </div> 
                                @empty
                                    Belum ada komplain
                                @endforelse


                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="card mb-4">
                        <div class="card-block">
                            
                            <h4 class="card-title">Komplain Terbaru</h4>
                            <ul class="timeline">
                               @foreach ($newest_complaitns as $item)
                                   
                               <li>
                                   <div class="timeline-badge primary"><em class="fa fa-link"></em></div>
                                   <div class="timeline-panel">
                                       <div class="timeline-heading">
                                            <a href="#">
                                               <h5 class="timeline-title mt-2">{!! $item->complaint_subject !!}</h5>
                                            </a>
                                        </div>
                                        <div class="timeline-body">
                                            <p>{!! $item->complaint_body !!}.</p>
                                        </div>
                                    </div>
                                </li>
                                
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </section>
            <section class="row">
                <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
            </section>
        </div>
    </section>
@endsection