<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusComplaint extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function complaint()
    {
        return $this->hasMany('App\Models\Complaint');
    }

}
