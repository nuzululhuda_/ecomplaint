<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = [
        'created_at'
    ];

    public function attachment()
    {
        return $this->hasMany('App\Model\ComplaintAttachment');
    }

    public function comment()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function statusComplaint()
    {
        return $this->belongsTo('App\Models\StatusComplaint');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
