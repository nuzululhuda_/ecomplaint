<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'password', 'user_level_id', 'email', 'api_token',
                            'created_at', 'updated_at', 'position_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function userLevel()
    {
        return $this->belongsTo('App\Models\UserLevel');
    }
    
    public function complaint()
    {
        return $this->hasMany('App\Models\Complaint');
    }

    public function hasLevel($roleName)
    {
        foreach ($this->userLevel as $role)
        {
            if ($role->level === $roleName) return true;
        }
            return false;
    }

    public function position()
    {
        return $this->hasOne('App\Models\Position');
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
