<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['level', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->hasMany('App\Models\User');
    }
}
