<?php

namespace App\Http\Controllers;

use DB;
use Image;
use Purifier;
use Validator;
use App\Models\Complaint;
use App\Models\Position;
use Illuminate\Http\Request;
use App\Models\StatusComplaint;
use Illuminate\Support\Facades\Auth;


class MahasiswaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return void
     */
    public function myComplaints()
    {
        $user_id = Auth::user()->id;
        $complaints = DB::table('complaints')
            ->join('users', 'complaints.user_id', '=', 'users.id')
            ->select(
                'users.name', 'complaints.complaint_subject','complaints.complaint_body', 'complaints.id',
                'complaints.complaint_image', 'complaints.complaint_to', 'complaints.created_at'
                )
            ->where('complaints.user_id', $user_id)
            ->paginate(15);
        return view('mahasiswa.home', compact('complaints'));
    }

    /**
     * Form create a complaint 
     * 
     * @return void
     */
    public function createComplaint()
    {
        $complaint_to = DB::table('positions')
            ->select('position')
            ->where('position', '!=', 'Mahasiswa')
            ->get();
        return view('mahasiswa.create-complaint', compact('complaint_to'));
    }
    
    /**
     * store komplain dari form complain
     * 
     * @return void
     */
    public function storeComplaint(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'img' => 'required|image',
            'complaint_subject' => 'required|max:255',
            'complaint_body' => 'required',
            'tujuan' => 'required|max:255',
        ]);
        $tujuan = implode(", ", $request->tujuan);
        
        $filenamewithextension = $request->file('img')->getClientOriginalName();

        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
 
        $extension = $request->file('img')->getClientOriginalExtension();

        $filenametostore = $filename.'_'.time().'.'.$extension;
 
        $request->file('img')->storeAs('public/image/original/', $filenametostore);
        $request->file('img')->storeAs('public/image/thumbnail/', $filenametostore);
 
        $thumbnailpath = public_path('storage/image/thumbnail/'.$filenametostore);
        $img = Image::make($thumbnailpath)->resize(700, 600, function($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($thumbnailpath);
        $status = StatusComplaint::where('default_status', true)->first();
        
        $data = [
            'complaint_subject' => Purifier::clean($request->complaint_subject),
            'complaint_body' => Purifier::clean($request->complaint_body),
            'complaint_image' => $filenametostore,
            'complaint_to' => \strip_tags($tujuan),
            'user_id' => Auth::user()->id,
            'status_id' => $status->id,
        ];
        
        Complaint::create($data);
        
        return redirect()->route('mahasiswa.komplain')->with([
            'message' => 'Komplain berhasil di buat.'
        ]);
    }
    
    /**
     * show spesific komplain
     * 
     * @return void
     */
    public function showComplaint($id)
    {
        Complaint::where('id', $id)->firstOrFail();
        
        $complaint = DB::table('complaints')
            ->join('users', 'complaints.user_id', '=', 'users.id')
            ->join('status_complaints', 'complaints.status_id', '=', 'status_complaints.id')
            ->select(
                'users.name', 'complaints.complaint_subject','complaints.complaint_body', 'complaints.id',
                'complaints.complaint_image', 'complaints.complaint_to', 'complaints.created_at',
                'users.avatar', 'status_complaints.status'
                )
            ->where('complaints.id', $id)
            ->first();

        $comments = DB::table('comments')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->leftJoin('positions', 'users.position_id', '=', 'positions.id')
            ->where('complaint_id', $id)
            ->select(
                'comment_body', 'users.name', 'users.avatar', 'positions.position'
            )
            ->orderBy('comments.id', 'DESC')
            ->get();

        $comments->map(function ($item, $key) {
            if ($item->position === null) {
                return $item->position = 'Mahasiswa';
            }
        });

        $status = DB::table('status_complaints')
            ->select('id', 'status')
            ->get();
    
        return view('mahasiswa.show-complaint', compact('complaint', 'comments', 'status'));
    }
}
