<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Comment;
use App\Models\Complaint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store($id, Request $request)
    {
        $complaint = Complaint::where('id', $id)->firstOrFail();
        $validator = Validator::make($request->all(), [
            'comment_body' => 'required',
        ]);
        $data = [
            'comment_body' => $request->comment_body,
            'user_id' => Auth::user()->id,
            'complaint_id' => $id,
        ];
        Comment::create($data);
        
        if (Auth::user()->position_id == null)
            return redirect()->route('mahasiswa.show.complaint', $id);
        else
            return redirect()->route('pegawai.show.complaint', $id);
    }
}
