<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unanswered_complaints = DB::table('complaints')
                ->join('users', 'complaints.user_id', '=', 'users.id')
                ->select(
                    'users.name', 'complaints.complaint_subject','complaints.complaint_body',
                    'complaints.complaint_image', 'complaints.complaint_to', 'complaints.created_at',
                    'complaints.id'
                    )
                ->where('complaints.status_id', 1)
                ->orderBy('complaints.id', 'ASC')
                ->paginate(7);

        $newest_complaitns = DB::table('complaints')
                ->join('users', 'complaints.user_id', '=', 'users.id')
                ->select(
                    'users.name', 'complaints.complaint_subject','complaints.complaint_body',
                    'complaints.complaint_image', 'complaints.complaint_to', 'complaints.created_at',
                    'complaints.id'
                    )
                ->where('complaints.status_id', 1)
                ->limit(3)
                ->orderBy('complaints.id', 'DESC')
                ->get();
        return view('pegawai.home', compact('unanswered_complaints', 'newest_complaitns'));
    }
}
