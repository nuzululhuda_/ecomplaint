<?php

namespace App\Http\Middleware;

use Closure;

class UserLevelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! $request->user()->hasRole($roleName))
        {
            return redirect()
                ->to('home');
        }
        return $next($request);
    }
}
