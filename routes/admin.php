<?php
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'AdminController@index')->name('admin.dashboard');
Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');
Route::get('register', 'AdminController@create')->name('admin.register');
Route::post('register', 'AdminController@store')->name('admin.register.store');
Route::get('login', 'Auth\Admin\LoginController@login')->name('admin.auth.login');
Route::post('login', 'Auth\Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
Route::post('logout', 'Auth\Admin\LoginController@logout')->name('admin.auth.logout');