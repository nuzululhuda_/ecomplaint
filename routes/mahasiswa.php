<?php

/*
|--------------------------------------------------------------------------
| Mahasiswa Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/komplain-saya', 'MahasiswaController@myComplaints')->name('mahasiswa.komplain');
Route::get('/buat-komplain', 'MahasiswaController@createComplaint')->name('mahasiswa.create.complaint');
Route::post('/buat-komplain', 'MahasiswaController@storeComplaint')->name('mahasiswa.store.complaint');
Route::get('/detail-komplain/{id}', 'MahasiswaController@showComplaint')->name('mahasiswa.show.complaint');
Route::post('/store-comment/{id}', 'CommentController@store')->name('comment.store');