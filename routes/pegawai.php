<?php

/*
|--------------------------------------------------------------------------
| Pegawai Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PegawaiController@index')->name('pegawai.index');
Route::get('/detail-komplain/{id}', 'MahasiswaController@showComplaint')->name('pegawai.show.complaint');
Route::post('/store-comment/{id}', 'CommentController@store')->name('pegawai.comment.store');